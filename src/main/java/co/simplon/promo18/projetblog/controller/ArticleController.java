package co.simplon.promo18.projetblog.controller;

import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;
import co.simplon.promo18.projetblog.entities.Article;
import co.simplon.promo18.projetblog.entities.Comments;
import co.simplon.promo18.projetblog.repository.ArticleRepository;
import co.simplon.promo18.projetblog.repository.CommentsRepository;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
public class ArticleController {
  @Autowired
  ArticleRepository artRepo;
  @Autowired
  CommentsRepository comRepo;

  @GetMapping("/api/article")
  public List<Article> getAllArticles() {
    return artRepo.findAll();
  }

  @PostMapping("/api/article/{idCategory}")
  public Article saveArticle(@Valid @RequestBody Article article, @PathVariable int idCategory) {
    artRepo.SaveArticle(article, idCategory);
    return article;
  }
  @PostMapping("api/article/comment/{id}")
  public void saveCommentWithArticleId(@Valid @RequestBody Comments comments,@PathVariable int id){
      comRepo.SaveComments(comments, id);
  }

  @GetMapping("api/article/only/{id}")
  public Article getArticleOnly(@PathVariable int id) {
    return artRepo.getArticleByIdWithoutComments(id);
  }

  @GetMapping("api/article/{id}")
  public Article getArticleById(@PathVariable int id) {
    return artRepo.getArticleById(id);
  }

  @DeleteMapping("api/article/{id}")
  public void deleteArticle(@PathVariable int id) {
    artRepo.deleteArticle(id);
  }
  @DeleteMapping("api/article/comment/{id}")
  public void deleteAllCommentsOfAnArticle(@PathVariable int id){
    comRepo.deleteAllCommentsFromArticle(id);
  }

  @PutMapping("api/article/{id}")
  public Article updateArticle(@Valid @RequestBody Article article, @PathVariable int id) {
    if (id != article.getId()) {
      throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
    }
    if (!artRepo.updadeArticle(article)) {
      throw new ResponseStatusException(HttpStatus.NOT_FOUND);
    }
    return artRepo.getArticleById(article.getId());

  }

  @PatchMapping("api/article/{id}")
  public Article patch(@Valid @RequestBody Article article, @PathVariable int id) {
    Article baseArt = artRepo.getArticleByIdWithoutComments(id);
    if (baseArt == null)
      throw new ResponseStatusException(HttpStatus.NOT_FOUND);
    if (article.getTitle() != null){
    baseArt.setTitle(article.getTitle());
  }
    if (article.getContent() != null){
    baseArt.setContent((article.getContent()));
  }
    if (article.getAuthor() !=null){
    baseArt.setAuthor(article.getAuthor());
  }
  if (article.getDate() !=null){
    baseArt.setDate(article.getDate());
  }
    if (!artRepo.updadeArticle(baseArt))
      throw new ResponseStatusException(HttpStatus.NOT_FOUND);
    return baseArt;
  }

}
