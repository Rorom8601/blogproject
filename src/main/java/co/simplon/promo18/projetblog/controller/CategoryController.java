package co.simplon.promo18.projetblog.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import java.util.List;
import javax.validation.Valid;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;
import co.simplon.promo18.projetblog.entities.Category;
import co.simplon.promo18.projetblog.repository.CategoryRepository;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
public class CategoryController {

  @Autowired
  CategoryRepository catRepo;

  @GetMapping("/api/categoryAll")
  public List<Category> getAllCategoriesArticlesIncluded() {
    return catRepo.getAllCategoriesWithArticles();
  }

  @GetMapping("api/category")
  public List<Category> getAllCategories() {
    return catRepo.getAllCategories();
  }

  @GetMapping("api/category/{id}")
  public Category getCategoryById(@PathVariable int id) {
    return catRepo.getCategoryById(id);
  }

  @PostMapping("api/category")
  public Category saveCategory(@Valid @RequestBody Category category) {
    catRepo.save(category);
    return category;
  }

  @DeleteMapping("api/category/{id}")
  public void deleteById(@PathVariable int id) {
    catRepo.delete(id);

  }

  @PutMapping("api/category/update/{id}")
  public Category updateArticle(@Valid @RequestBody Category category, @PathVariable int id) {
    if (id != category.getId()) {
      throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
    }
    if (!catRepo.updadeCategory(category)) {
      throw new ResponseStatusException(HttpStatus.NOT_FOUND);
    }
    return catRepo.getCategoryById(category.getId());

  }

}
