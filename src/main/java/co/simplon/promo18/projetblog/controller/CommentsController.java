package co.simplon.promo18.projetblog.controller;

import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;
import co.simplon.promo18.projetblog.entities.Comments;
import co.simplon.promo18.projetblog.repository.CommentsRepository;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/api/comments")
public class CommentsController {
  @Autowired
  private CommentsRepository comRepo;

  @GetMapping
  public List<Comments> getAllComments() {
    return comRepo.getAll();
  }

  @GetMapping("/{id}")
  public Comments getCommentById(@PathVariable int id) {
    Comments comments = comRepo.getCommentById(id);
    return comments;
  }

  // @PostMapping
  //  //@ResponseStatus(HttpStatus.CREATED)
  // public Comments saveComment(@RequestBody Comments comments) {
  //   comRepo.SaveComments(comments);
  //   return comments;
  // }
  @GetMapping("/article/{idComment}")
  public List<Comments> getCommentByIdArticle(@PathVariable int idComment) {
   List<Comments> commentsList = comRepo.getCommentsByArticleId(idComment);
    return commentsList;
  }
  @DeleteMapping("/{id}")
  public void delete(@PathVariable int id){
    comRepo.deleteComment(id);
  }

  @PutMapping("/{id}")
  public Comments updateArticle(@Valid @RequestBody Comments comments, @PathVariable int id) {
    if (id != comments.getId()) {
      throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
    }
    if (!comRepo.updadeComments(comments)) {
      throw new ResponseStatusException(HttpStatus.NOT_FOUND);
  }
  return comRepo.getCommentById(comments.getId());

  }
}
