package co.simplon.promo18.projetblog.entities;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.PastOrPresent;

public class Article {
  private int id;
  @NotBlank
  private String title;
  @NotBlank
  private String content;
  @NotBlank
  private String author;
  @NotNull
  @PastOrPresent
  private LocalDate date;
  private List<Comments> listComments=new ArrayList<Comments>();
  public Article (){}
  public Article(String title, String content, String author, LocalDate date) {
    this.title = title;
    this.content = content;
    this.author = author;
    this.date = date;
  }
  public Article(int id, String title, String content, String author, LocalDate date) {
    this.id = id;
    this.title = title;
    this.content = content;
    this.author = author;
    this.date = date;
  }
  public Article(int id, String title, String content, String author, LocalDate date,
      List<Comments> listComments) {
    this.id = id;
    this.title = title;
    this.content = content;
    this.author = author;
    this.date = date;
    this.listComments = listComments;
  }
  public int getId() {
    return id;
  }
  public void setId(int id) {
    this.id = id;
  }
  public String getTitle() {
    return title;
  }
  public void setTitle(String title) {
    this.title = title;
  }
  public String getContent() {
    return content;
  }
  public void setContent(String content) {
    this.content = content;
  }
  public String getAuthor() {
    return author;
  }
  public void setAuthor(String author) {
    this.author = author;
  }
  public LocalDate getDate() {
    return date;
  }
  public void setDate(LocalDate date) {
    this.date = date;
  }
  public List<Comments> getListComments() {
    return listComments;
  }
  public void setListComments(List<Comments> listComments) {
    this.listComments = listComments;
  }
}
