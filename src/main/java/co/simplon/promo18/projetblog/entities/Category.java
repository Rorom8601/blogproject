package co.simplon.promo18.projetblog.entities;

import java.util.ArrayList;
import java.util.List;
import javax.validation.constraints.NotBlank;

public class Category {
  private int id;
  @NotBlank
  private String label;
  private List<Article> listArticle=new ArrayList<Article>();

  public Category(){}
  
  public Category(String label) {
    this.label = label;
  }
  public Category(int id, String label) {
    this.id = id;
    this.label = label;
  }
  public Category(int id, String label, List<Article> listArticle) {
    this.id = id;
    this.label = label;
    this.listArticle = listArticle;
  }
  public int getId() {
    return id;
  }
  public void setId(int id) {
    this.id = id;
  }
  public String getLabel() {
    return label;
  }
  public void setLabel(String label) {
    this.label = label;
  }
  public List<Article> getListArticle() {
    return listArticle;
  }
  public void setListArticle(List<Article> listArticle) {
    this.listArticle = listArticle;
  }
}
