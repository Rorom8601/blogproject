package co.simplon.promo18.projetblog.entities;

import javax.validation.constraints.NotBlank;

public class Comments {
  private int id;
  @NotBlank
  private String label;
  
  public Comments(String text) {
    this.label = text;
  }
  public Comments(int id, String text) {
    this.id = id;
    this.label = text;
  }
  public int getId() {
    return id;
  }
  public void setId(int id) {
    this.id = id;
  }
  public String getLabel() {
    return label;
  }
  public void setLabel(String label) {
    this.label = label;
  }
}
