package co.simplon.promo18.projetblog.repository;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.RestController;
import co.simplon.promo18.projetblog.entities.Article;
import co.simplon.promo18.projetblog.entities.Comments;

@Repository
public class ArticleRepository {

  @Autowired
  private DataSource dataSource;
  @Autowired
  CommentsRepository comRepo;

  public List<Article> findAll() {
    List<Article> list = new ArrayList<>();
    try (Connection connection = dataSource.getConnection()) {
      PreparedStatement stmt = connection.prepareStatement("SELECT * FROM article");

      ResultSet rs = stmt.executeQuery();

      while (rs.next()) {
        Article article = new Article(rs.getInt("id"), rs.getString("title"),
            rs.getString("content"), rs.getString("author"), rs.getDate("date").toLocalDate());
        List<Comments> listComments = comRepo.getCommentsByArticleId(rs.getInt("id"));
        article.setListComments(listComments);
        list.add(article);
      }
    } catch (SQLException e) {

      e.printStackTrace();
    }

    return list;
  }

  public Article getArticleByIdWithoutComments(int id) {
    try (Connection connection = dataSource.getConnection()) {
      PreparedStatement stmt =
          connection.prepareStatement("SELECT * FROM article WHERE article.id=?");
      stmt.setInt(1, id);
      ResultSet rs = stmt.executeQuery();

      while (rs.next()) {
        Article article = new Article(rs.getInt("id"), rs.getString("title"),
            rs.getString("content"), rs.getString("author"), rs.getDate("date").toLocalDate());
        return article;
      }
    } catch (SQLException e) {

      e.printStackTrace();
    }
    return null;
  }

  public Article getArticleById(int id) {
    try (Connection connection = dataSource.getConnection()) {
      PreparedStatement stmt =
          connection.prepareStatement("SELECT * FROM article WHERE article.id=?");
      stmt.setInt(1, id);
      ResultSet rs = stmt.executeQuery();

      while (rs.next()) {
        Article article = new Article(rs.getInt("id"), rs.getString("title"),
            rs.getString("content"), rs.getString("author"), rs.getDate("date").toLocalDate());
        List<Comments> listComments = comRepo.getCommentsByArticleId(rs.getInt("id"));
        article.setListComments(listComments);
        return article;
      }
    } catch (SQLException e) {

      e.printStackTrace();
    }
    return null;
  }

  public void SaveArticle(Article article, int categoryId) {

    try (Connection connection = dataSource.getConnection()) {
      PreparedStatement stmt = connection.prepareStatement(
          "INSERT INTO article (title, content, author, date, category_id) VALUES (?,?,?,?,?)",
          PreparedStatement.RETURN_GENERATED_KEYS);

      stmt.setString(1, article.getTitle());
      stmt.setString(2, article.getContent());
      stmt.setString(3, article.getAuthor());
      stmt.setDate(4, Date.valueOf(article.getDate()));
      stmt.setInt(5, categoryId);

      stmt.executeUpdate();

      ResultSet rs = stmt.getGeneratedKeys();
      if (rs.next()) {
        article.setId(rs.getInt(1));
      }

    } catch (SQLException e) {

      e.printStackTrace();
    }

  }

  public List<Article> getArticleListByCategoryId(int id) {
    List<Article> list = new ArrayList<>();
    try (Connection connection = dataSource.getConnection()) {
      PreparedStatement stmt = connection.prepareStatement(
          "SELECT * FROM article INNER JOIN category ON category.id=article.category_id WHERE category.id=?");
      stmt.setInt(1, id);
      ResultSet rs = stmt.executeQuery();

      while (rs.next()) {
        Article article = new Article(rs.getInt("id"), rs.getString("title"),
            rs.getString("content"), rs.getString("author"), rs.getDate("date").toLocalDate());
        List<Comments> listComments = comRepo.getCommentsByArticleId(rs.getInt("id"));
        article.setListComments(listComments);
        list.add(article);
      }
    } catch (SQLException e) {

      e.printStackTrace();
    }

    return list;
  }

  public void deleteArticle(int id) {
    try (Connection connection = dataSource.getConnection()) {
      PreparedStatement stmt = connection.prepareStatement("DELETE FROM article WHERE id=?");
      stmt.setInt(1, id);
      stmt.executeUpdate();
      // if(rs.next()){
      // return true;
      // }
    } catch (SQLException e) {

      e.printStackTrace();
    }
  }

  public boolean updadeArticle(Article article) {
    try (Connection connection = dataSource.getConnection()) {
      PreparedStatement stmt = connection.prepareStatement(
          "UPDATE article SET title = ?, content = ?, author = ?, date=? WHERE id = ?");

      stmt.setString(1, article.getTitle());
      stmt.setString(2, article.getContent());
      stmt.setString(3, article.getAuthor());
      stmt.setDate(4, Date.valueOf(article.getDate()));
      stmt.setInt(5, article.getId());


      return (stmt.executeUpdate() == 1);

    } catch (SQLException e) {

      e.printStackTrace();
      throw new RuntimeException("Database access error");
    }

  }
}


