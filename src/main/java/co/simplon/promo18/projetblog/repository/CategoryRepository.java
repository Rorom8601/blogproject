package co.simplon.promo18.projetblog.repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import co.simplon.promo18.projetblog.entities.Article;
import co.simplon.promo18.projetblog.entities.Category;

@Repository
public class CategoryRepository {
  @Autowired
  DataSource dataSource;
  @Autowired
  ArticleRepository artRepo;

  public List<Category> getAllCategoriesWithArticles() {
    List<Category> list = new ArrayList<>();
    try (Connection connection = dataSource.getConnection()) {
      PreparedStatement stmt = connection.prepareStatement("SELECT * FROM category");

      ResultSet rs = stmt.executeQuery();

      while (rs.next()) {
        Category category = new Category(rs.getInt("id"), rs.getString("label"));

        List<Article> listArticle = artRepo.getArticleListByCategoryId(rs.getInt("id"));
        category.setListArticle(listArticle);
        list.add(category);
      }
    } catch (SQLException e) {
      e.printStackTrace();
    }
    return list;
  }

  public List<Category> getAllCategories() {
    List<Category> list = new ArrayList<>();
    try (Connection connection = dataSource.getConnection()) {
      PreparedStatement stmt = connection.prepareStatement("SELECT * FROM category");

      ResultSet rs = stmt.executeQuery();

      while (rs.next()) {
        Category category = new Category(rs.getInt("id"), rs.getString("label"));
        list.add(category);
      }
    } catch (SQLException e) {
      e.printStackTrace();
    }
    return list;
  }

  public Category getCategoryById(int id) {
    try (Connection connection = dataSource.getConnection()) {
      PreparedStatement stmt =
          connection.prepareStatement("SELECT * FROM category WHERE category.id=?");
      stmt.setInt(1, id);
      ResultSet rs = stmt.executeQuery();

      while (rs.next()) {
        Category category = new Category(rs.getInt("id"), rs.getString("label"));
        List<Article> listArticle = artRepo.getArticleListByCategoryId(rs.getInt("id"));
        category.setListArticle(listArticle);
        return category;
      }
    } catch (SQLException e) {

      e.printStackTrace();
    }
    return null;
  }

  public void save(Category category) {
    try (Connection connection = dataSource.getConnection()) {
      PreparedStatement stmt = connection.prepareStatement(
          "INSERT INTO category (label) VALUES (?)", PreparedStatement.RETURN_GENERATED_KEYS);

      stmt.setString(1, category.getLabel());
      stmt.executeUpdate();

      ResultSet rs = stmt.getGeneratedKeys();
      if (rs.next()) {
        category.setId(rs.getInt(1));
      }

    } catch (SQLException e) {

      e.printStackTrace();
    }

  }

  public void delete(int id) {
    try (Connection connection = dataSource.getConnection()) {
      PreparedStatement stmt = connection.prepareStatement("DELETE FROM category WHERE id=?");
      stmt.setInt(1, id);
      stmt.executeUpdate();
      // if(rs.next()){
      // return true;
      // }
    } catch (SQLException e) {

      e.printStackTrace();
    }
  }
  public boolean updadeCategory(Category category) {
    try (Connection connection = dataSource.getConnection()) {
      PreparedStatement stmt = connection.prepareStatement(
          "UPDATE category SET label = ? WHERE id = ?");

      stmt.setString(1, category.getLabel());
      stmt.setInt(2, category.getId());
      
      return (stmt.executeUpdate() == 1);

    } catch (SQLException e) {

      e.printStackTrace();
      throw new RuntimeException("Database access error");
    }

  }
}

