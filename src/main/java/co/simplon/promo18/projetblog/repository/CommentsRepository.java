package co.simplon.promo18.projetblog.repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import co.simplon.promo18.projetblog.entities.Comments;

@Repository
public class CommentsRepository {
  @Autowired
  private DataSource dataSource;


  public List<Comments> getAll() {
    List<Comments> list = new ArrayList<>();
    try (Connection connection = dataSource.getConnection()) {
      PreparedStatement stmt = connection.prepareStatement("SELECT * FROM comments");

      ResultSet rs = stmt.executeQuery();

      while (rs.next()) {
        Comments comments = new Comments(rs.getInt("id"), rs.getString("content"));
        list.add(comments);
      }
    } catch (SQLException e) {
      e.printStackTrace();
    }
    return list;
  }

  public Comments getCommentById(int id) {

    try (Connection connection = dataSource.getConnection()) {
      PreparedStatement stmt = connection.prepareStatement("SELECT * FROM comments WHERE id=?");
      stmt.setInt(1, id);
      ResultSet rs = stmt.executeQuery();

      if (rs.next()) {
        Comments comments = new Comments(rs.getInt("id"), rs.getString("content"));
        return comments;
      }
    } catch (SQLException e) {
      e.printStackTrace();
    }
    return null;
  }

  public void SaveComments(Comments comments, int idArticle) {
    try (Connection connection = dataSource.getConnection()) {
      PreparedStatement stmt = connection.prepareStatement(
          "INSERT INTO comments (content, article_id) VALUES (?,?)",PreparedStatement.RETURN_GENERATED_KEYS);

      stmt.setString(1, comments.getLabel());
      stmt.setInt(2, idArticle);
      stmt.executeUpdate();

      ResultSet rs = stmt.getGeneratedKeys();
      if (rs.next()) {
        comments.setId(rs.getInt(1));
      }

    } catch (SQLException e) {

      e.printStackTrace();
    }
  }

  public List<Comments> getCommentsByArticleId(int id) {
    List<Comments> list = new ArrayList<>();
    try (Connection connection = dataSource.getConnection()) {
      PreparedStatement stmt = connection.prepareStatement("select * from comments INNER JOIN article ON article.id=comments.article_id where article.id=?");
      stmt.setInt(1,id);
      ResultSet rs = stmt.executeQuery();

      while (rs.next()) {
        Comments comments = new Comments(rs.getInt("id"), rs.getString("content"));
        list.add(comments);
      }
    } catch (SQLException e) {
      e.printStackTrace();
    }
    return list;
  }
  public void deleteComment(int id){
    try (Connection connection = dataSource.getConnection()) {
      PreparedStatement stmt =
          connection.prepareStatement("DELETE FROM comments WHERE id=?");
      stmt.setInt(1, id);
      stmt.executeUpdate();
    } catch (SQLException e) {

      e.printStackTrace();
    }
  }
  public void deleteAllCommentsFromArticle(int id){
    try (Connection connection = dataSource.getConnection()) {
      PreparedStatement stmt =
          connection.prepareStatement("DELETE FROM comments WHERE article_id=?");
      stmt.setInt(1, id);
      stmt.executeUpdate(); 
    } catch (SQLException e) {

      e.printStackTrace();
    }
  }
  public boolean updadeComments(Comments comments) {
    try (Connection connection = dataSource.getConnection()) {
      PreparedStatement stmt = connection.prepareStatement(
          "UPDATE comments SET content = ? WHERE id = ?");

      stmt.setString(1, comments.getLabel());
      stmt.setInt(2, comments.getId());
      
      return (stmt.executeUpdate() == 1);

    } catch (SQLException e) {

      e.printStackTrace();
      throw new RuntimeException("Database access error");
    }

  }

}
