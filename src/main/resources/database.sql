CREATE DATABASE blog;

USE blog;

CREATE TABLE article(
    id INT PRIMARY KEY AUTO_INCREMENT,
    title VARCHAR(155),
    content VARCHAR(6535),
    author VARCHAR(155),
    date DATE
);

CREATE TABLE category(
    id INT PRIMARY KEY AUTO_INCREMENT,
    label VARCHAR(155)
);

CREATE TABLE comments(
    id INT PRIMARY KEY AUTO_INCREMENT,
    content TEXT
);

ALTER TABLE article MODIFY content TEXT;

ALTER TABLE
    article
ADD
    CONSTRAINT FK_category FOREIGN KEY (id) REFERENCES category(id);

ALTER TABLE
    category
ADD
    CONSTRAINT FK_article FOREIGN KEY (id) REFERENCES article(id);

ALTER TABLE article ADD COLUMN category_id INT;

ALTER TABLE article DROP CONSTRAINT FK_category;

ALTER TABLE category DROP CONSTRAINT FK_article;

ALTER TABLE
    article
ADD
    CONSTRAINT FK_category FOREIGN KEY (category_id) REFERENCES category(id);

ALTER TABLE comments ADD COLUMN article_id INT;

ALTER TABLE
    comments
ADD
    CONSTRAINT FK_article FOREIGN KEY (article_id) REFERENCES article(id);

/*INSERTION D'UN JEU D'ESSAI*/

INSERT INTO
    category(label)
VALUES
    ('humour'),
    ('divers'),('politique'),
    ('sport');

INSERT INTO
    article(title, author, content, date, category_id)
VALUES
    (
        'Un homme fait 3 fois le tour du monde en rampant',
        'Charlie Martel',
        'Lorem, ipsum dolor sit amet consectetur adipisicing elit. Aliquid sequi laborum nemo, pariatur accusantium, eveniet velit debitis natus labore, veniam vero rem vitae totam dolores ut eius officia dolorum in.Lorem, ipsum dolor sit amet consectetur adipisicing elit. Aliquid sequi laborum nemo, pariatur accusantium, eveniet velit debitis natus labore, veniam vero rem vitae totam dolores ut eius officia dolorum in.Lorem, ipsum dolor sit amet consectetur adipisicing elit. Aliquid sequi laborum nemo, pariatur accusantium, eveniet velit debitis natus labore, veniam vero rem vitae totam dolores ut eius officia dolorum in.',
        '2022-02-02',
        1
    ),
    (
        'Manger enfin 5 fruits et légumes par jour avec nos tomates cerises',
        'Marlene Jsaispas',
        'Lorem, ipsum dolor sit amet consectetur adipisicing elit. Aliquid sequi laborum nemo, pariatur accusantium, eveniet velit debitis natus labore, veniam vero rem vitae totam dolores ut eius officia dolorum in.Lorem, ipsum dolor sit amet consectetur adipisicing elit. Aliquid sequi laborum nemo, pariatur accusantium, eveniet velit debitis natus labore, veniam vero rem vitae totam dolores ut eius officia dolorum in.Lorem, ipsum dolor sit amet consectetur adipisicing elit. Aliquid sequi laborum nemo, pariatur accusantium, eveniet velit debitis natus labore, veniam vero rem vitae totam dolores ut eius officia dolorum in.',
        '2021-02-02',
        2
    ),
    (
        'Comment maigrir en mangeant de la raclette',
        'Gerard Larcher',
        'Lorem, ipsum dolor sit amet consectetur adipisicing elit. Aliquid sequi laborum nemo, pariatur accusantium, eveniet velit debitis natus labore, veniam vero rem vitae totam dolores ut eius officia dolorum in.Lorem, ipsum dolor sit amet consectetur adipisicing elit. Aliquid sequi laborum nemo, pariatur accusantium, eveniet velit debitis natus labore, veniam vero rem vitae totam dolores ut eius officia dolorum in.',
        '2022-10-02',
        2
    ),
    (
        'Participez à la cagnotte pour les Balkany!!',
        'Julie Laroux',
        'Lorem, ipsum dolor sit amet consectetur adipisicing elit. Aliquid sequi laborum nemo, pariatur accusantium, eveniet velit debitis natus labore, veniam vero rem vitae totam dolores ut eius officia dolorum in.Lorem, ipsum dolor sit amet consectetur adipisicing elit. Aliquid sequi laborum nemo, pariatur accusantium, eveniet velit debitis natus labore, veniam vero rem vitae totam dolores ut eius officia dolorum in.Lorem, ipsum dolor sit amet consectetur adipisicing elit. Aliquid sequi laborum nemo, pariatur accusantium, eveniet velit debitis natus labore, veniam vero rem vitae totam dolores ut eius officia dolorum in.',
        '2022-03-02',
        3
    ),
    (
        'Record du monde de cure-dent empillés battus',
        'Charlie Martel',
        'Lorem, ipsum dolor sit amet consectetur adipisicing elit. Aliquid sequi laborum nemo, pariatur accusantium, eveniet velit debitis natus labore, veniam vero rem vitae totam dolores ut eius officia dolorum in.Lorem, ipsum dolor sit amet consectetur adipisicing elit. Aliquid sequi laborum nemo, pariatur accusantium, eveniet velit debitis natus labore, veniam vero rem vitae totam dolores ut eius officia dolorum in.Lorem, ipsum dolor sit amet consectetur adipisicing elit. Aliquid sequi laborum nemo, pariatur accusantium, eveniet velit debitis natus labore, veniam vero rem vitae totam dolores ut eius officia dolorum in.',
        '2022-04-10',
        2
    ),
    (
        'Pourquoi Federer est le meilleur joueur de tennis?',
        'Benoit Jacques',
        'Lorem, ipsum dolor sit amet consectetur adipisicing elit. Aliquid sequi laborum nemo, pariatur accusantium, eveniet velit debitis natus labore, veniam vero rem vitae totam dolores ut eius officia dolorum in.Lorem, ipsum dolor sit amet consectetur adipisicing elit. Aliquid sequi laborum nemo, pariatur accusantium, eveniet velit debitis natus labore, veniam vero rem vitae totam dolores ut eius officia dolorum in.Lorem, ipsum dolor sit amet consectetur adipisicing elit. Aliquid sequi laborum nemo, pariatur accusantium, eveniet velit debitis natus labore, veniam vero rem vitae totam dolores ut eius officia dolorum in.',
        '2021-12-02',
        4
    ),
    (
        'Apprenez la programmation pendant votre sommeil',
        'François Stein',
        'Lorem, ipsum dolor sit amet consectetur adipisicing elit. Aliquid sequi laborum nemo, pariatur accusantium, eveniet velit debitis natus labore, veniam vero rem vitae totam dolores ut eius officia dolorum in.Lorem, ipsum dolor sit amet consectetur adipisicing elit. Aliquid sequi laborum nemo, pariatur accusantium, eveniet velit debitis natus labore, veniam vero rem vitae totam dolores ut eius officia dolorum in.Lorem, ipsum dolor sit amet consectetur adipisicing elit. Aliquid sequi laborum nemo, pariatur accusantium, eveniet velit debitis natus labore, veniam vero rem vitae totam dolores ut eius officia dolorum in.',
        '2022-03-10',
        1
    );

INSERT INTO
    comments (content, article_id)
VALUES
    ("article très bien documenté, j'adore", 1),
    ("article très bien documenté, j'adore", 2),
    (
        "très instructif et efficace, j'ai commencé hier, j'ai déja perdu 8 kg",
        3
    ),
    (
        "J'ai vendu ma maison pour participer, je ne regrètte pas",
        4
    ),
    ("Belle initiative! ", 5),
    ("article non partisan et éclairant", 6),
    ("Je m'y mets dès la nuit prochaine", 7);